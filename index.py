import string
import sys 
import time

# x='10 0 17 5'
# a = x.split(' ')
# first = a.pop(0)
# print(first)
# print(a)
# c  = a.sort(key=int)
# print(c)
# d  = sorted(a)
# print(d)
# e  = a.sort()
# print(e)
# f  = a.reverse()
# print(f)


# sys.exit('die')


# Tests
def run_tests():

    print('------------------------------------------------------------')
    print('Testing functions')
    print('------------------------------------------------------------')
    

    # Test: valid_steps (r1, c1)
    print('\n') 
    print('------------------------------------------------------------')
    print('Testing function valid_steps (r1, c1)')
    print('\n')
    print("Position (1,1) expected 2 points: {'23': [2, 3], '32': [3, 2]}")
    print(valid_steps(1,1))
    time.sleep(2)
    print('\n')
    print("Position (3,3) expected 8 points: {'12': [1, 2], '14': [1, 4], '21': [2, 1], '25': [2, 5], '41': [4, 1], '45': [4, 5], '52': [5, 2], '54': [5, 4]}")
    print(valid_steps(3,3))
    time.sleep(2)

    print('------------------------------------------------------------')    
    print('\n')    



    # Test: get_map_coordinates()
    print('\n') 
    print('------------------------------------------------------------')    
    print('Testing function get_map_coordinates ()')
    print('Expect to receive an array of mapped coordinates: Cell numbers [0-63], alpha [A-H], RC [1-8]x[1-8] ')
    print(get_map_coordinates())
    time.sleep(2)
    print('\n')

    coordinatesMap=get_map_coordinates()
    print("Testing mapSquareRC = coordinatesMap['coordinatesMap']")
    mapSquareRC = coordinatesMap['mapSquareRC']
    print(mapSquareRC)
    print('\n')

    print("Testing mapSquareRC['0']. Expect to receive 11")
    print(mapSquareRC['0'])
    time.sleep(2)
    print('\n')

    print("Testing mapSquareRC'33']. Expect to receive 52")
    print(mapSquareRC['33'])
    time.sleep(2)
    print('\n')

    print("Testing charMap = coordinatesMap[charMap]")
    charMap     = coordinatesMap['charMap']
    print(charMap)
    time.sleep(2)
    print('\n')

    print("Testing charMap['0']. Expect to receive A1")
    print(charMap['0'])
    time.sleep(2)
    print('\n')

    print("Testing charMap['33']. Expect to receive B5")
    print(charMap['33'])
    time.sleep(2)
    

    print('------------------------------------------------------------')    
    print('\n')


    # Test: processing_prompt_data(x)
    print('\n') 
    print('------------------------------------------------------------')  
    print('processing_prompt_data(x)')
    print("Fake user input '10 0 17'. Expected to receive {'knightSquare': '10', 'pawnsSquare': ['0', '17']}")
    print(processing_prompt_data('10 0 17'))
    time.sleep(2)
    print('\n')    
    print('processing_prompt_data(x)')
    print("Fake user input '10 0 17 20 44 50 50 20 '. Expected to receive {'knightSquare': '10', 'pawnsSquare': ['0 17 20 44 50']}")
    print(processing_prompt_data('10 0 17 20 44 50 50 20 '))
    time.sleep(2)
    print('------------------------------------------------------------')    
    print('\n')


    # More tests

    
    print('\n')
    print("End of tests")

 

def valid_steps (r1, c1):
    # Pass trough RC-[1-8]x[1-8] coordinates and return an dictionary of knight valid steps
    # A valid knigth step should folow this rule: |r1-r2|+|c1-c2|=3 whith r1!=r2 and c1!=c2. http://www.inf.ufsc.br/grafos/temas/hamiltoniano/cavalo.htm
    validSteps={}
    for r2 in range(1,9):
        for c2 in range (1,9):            
            if (abs(r1-r2)+abs(c1-c2)==3) and r1!=r2 and c1!=c2: # Valid Knight mouvement
                validSteps[str(r2)+str(c2)]=[r2,c2]
    
    return validSteps



def processing_prompt_data(x):
    
    userInput = x.split(' ')
    piecesPositions={}
    knightPositions={}
    pawnsPositions={}
    coordinatesMap=get_map_coordinates()

    knightPositionSq = userInput.pop(0)
    pawnsPositionSq  = userInput

    mapSquareRC = coordinatesMap['mapSquareRC']
    charMap     = coordinatesMap['charMap']

    # knight
    knightPositions['square']=knightPositionSq
    knightPositions['rc']=mapSquareRC[knightPositionSq]
    knightPositions['alpha']=charMap[knightPositionSq]
    

    # pawns    
    pawnsSq={}
    pawnsRC={}
    pawnsAlpha={}
    for pawnsSquares in pawnsPositionSq:
        if pawnsSquares is not '': # Cleaning noises (empty spaces, repeated values). Ex: Enter at least 2 numbers between 0-63>10    20   30   40
            pawnsSq[pawnsSquares]=0 # Not captured - 0. Captured - 1.
            pawnsRC[mapSquareRC[pawnsSquares]]=0 # Mapping RC coordiates
            pawnsAlpha[charMap[pawnsSquares]]=0


    pawnsRC = dict(sorted(pawnsRC.items())) # Sort asc
    pawnsLength = len(pawnsRC)
    
    pawnsPositions['square']=pawnsSq
    pawnsPositions['rc']=pawnsRC
    pawnsPositions['alpha']=pawnsAlpha
    pawnsPositions['Length']=pawnsLength
    


    piecesPositions['knightPositions']=knightPositions
    piecesPositions['pawnsSquare']=pawnsPositions
    
    return piecesPositions


def get_map_coordinates():
    # 1 - Initialization: Map square number (0-63) to RC[1-8] (Row-Col) coordinates. {'0': 11, '1': 12, ... and apha coodinate [A-H]
    coordinatesMap={}  
    mapSquareRC={}
    charMap={}
    characters = string.ascii_uppercase
    sq=0
    for mapR in range(1,9):
        for mapC in range (1,9):
            rc=int(str(mapR)+str(mapC))
            mapSquareRC[str(sq)]=rc
            charMap[str(sq)]=str(characters[mapC-1])+str(mapR)
            sq += 1  
    coordinatesMap['mapSquareRC'] =   mapSquareRC     
    coordinatesMap['charMap']     =   charMap  
    return   coordinatesMap        



# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------


# 1 - Initialization: Map square number (0-63) to RC[1-8] (Row-Col) coordinates. {'0': 11, '1': 12, ...  
mapSquareRC={}
charMap={}
characters = string.ascii_uppercase
sq=0
for mapR in range(1,9):
    for mapC in range (1,9):
        rc=int(str(mapR)+str(mapC))
        mapSquareRC[str(sq)]=rc
        charMap[str(sq)]=str(characters[mapC-1])+str(mapR)
        sq += 1    
# ------------------------------------------------------------


# 2 - Prompt
#x = input('Enter at least 2 numbers between 0-63>')
#
x = '10 0 17' # ====================================================================> To be deleted



userInput = x.split(' ')
knightSquare = userInput.pop(0)
pawnsSquare   = userInput
print('Knight starts in the square - ' + knightSquare)
# Mapping Square to RC coordinates
knightStartRC = mapSquareRC[knightSquare]
r1=int(str(knightStartRC)[0]) # Get RC knight coordinate r1
c1=int(str(knightStartRC)[1]) # Get RC knight coordinate c1


pawns={}
pawnsRC={}
for pawnsSquares in userInput:
    if pawnsSquares is not '': # Cleaning noises (empty spaces, repeated values). Ex: Enter at least 2 numbers between 0-63>10    20   30   40
        pawns[pawnsSquares]=0 # Not captured - 0. Captured - 1.
        pawnsRC[mapSquareRC[pawnsSquares]]=0 # Mapping RC coordiates

pawnsRC = dict(sorted(pawnsRC.items())) # Sort asc

pawnsRCLength = len(pawnsRC)
# ------------------------------------------------------------


# 3 - Tour and capture
path=[]
path.append({v:k for k, v in mapSquareRC.items()}[int(knightStartRC)])
rp=0
cp=0
while sum(pawnsRC.values()) < pawnsRCLength:        
    for r2 in range(1,9):
        for c2 in range (1,9):            
            if (abs(r1-r2)+abs(c1-c2)==3) and r1!=r2 and c1!=c2 and r2!=rp and c2!=cp: # Valid Knight mouvement
                r2c2 = str(r2)+str(c2)
                rp=r1
                cp=c1
                r1=r2
                c1=c2    
                nbNotation = {v:k for k, v in mapSquareRC.items()}[int(r2c2)]
                AlphaNumNotation = charMap[str(nbNotation)]   
                path.append(str(nbNotation) + '-' + AlphaNumNotation)

                if int(r2c2) in  pawnsRC: # Capture pawns                    
                    pawnsRC[int(r2c2)]=1
                    if sum(pawnsRC.values()) < pawnsRCLength:
                        break
                    #time.sleep(5)
# ------------------------------------------------------------  
 

# 4 - Display                   
path.pop(0)
print ('Tour:')
for squares in path:
    print (squares)
# ------------------------------------------------------------  














    
    


