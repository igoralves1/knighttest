import string
import sys 
import time
import random

# Tests
def run_tests():

    print('------------------------------------------------------------')
    print('Testing functions')
    print('------------------------------------------------------------')


    
    # Test: get_board()
    print('\n') 
    print('------------------------------------------------------------')
    print('Testing function get_board()')
    print('\n')
    print(get_board())
    time.sleep(2)
    print('------------------------------------------------------------')    
    print('\n') 

    # Test: valid_steps (r1, c1)
    print('\n') 
    print('------------------------------------------------------------')
    print('Testing function valid_steps (r1, c1)')
    print('\n')
    print("Position (1,1) expected 2 points: {'23': [2, 3], '32': [3, 2]}")
    print(valid_steps(1,1))
    time.sleep(2)
    print('\n')
    print("Position (3,3) expected 8 points: {'12': [1, 2], '14': [1, 4], '21': [2, 1], '25': [2, 5], '41': [4, 1], '45': [4, 5], '52': [5, 2], '54': [5, 4]}")
    print(valid_steps(3,3))
    time.sleep(2)

    print('------------------------------------------------------------')    
    print('\n')    



    # Test: get_map_coordinates()
    print('\n') 
    print('------------------------------------------------------------')    
    print('Testing function get_map_coordinates ()')
    print('Expect to receive an array of mapped coordinates: Cell numbers [0-63], alpha [A-H], RC [1-8]x[1-8] ')
    print(get_map_coordinates())
    time.sleep(2)
    print('\n')

    coordinatesMap=get_map_coordinates()
    print("Testing mapSquareRC = coordinatesMap['coordinatesMap']")
    mapSquareRC = coordinatesMap['mapSquareRC']
    print(mapSquareRC)
    print('\n')

    print("Testing mapSquareRC['0']. Expect to receive 11")
    print(mapSquareRC['0'])
    time.sleep(2)
    print('\n')

    print("Testing mapSquareRC'33']. Expect to receive 52")
    print(mapSquareRC['33'])
    time.sleep(2)
    print('\n')

    print("Testing charMap = coordinatesMap[charMap]")
    charMap     = coordinatesMap['charMap']
    print(charMap)
    time.sleep(2)
    print('\n')

    print("Testing charMap['0']. Expect to receive A1")
    print(charMap['0'])
    time.sleep(2)
    print('\n')

    print("Testing charMap['33']. Expect to receive B5")
    print(charMap['33'])
    time.sleep(2)
    

    print('------------------------------------------------------------')    
    print('\n')


    # Test: processing_prompt_data(x)
    print('\n') 
    print('------------------------------------------------------------')  
    print('processing_prompt_data(x)')
    print("Fake user input '10 0 17'. Expected to receive {'knightPositions': '10', 'pawnsPositions': ['0', '17']}")
    piecesPositions = processing_prompt_data('10 0 17 5')
    print(piecesPositions)
    time.sleep(2)
    print('\n')    

    print("Testing piecesPositions['knightPositions']['square'] from previous input. Expect to receive 10")    
    print(piecesPositions['knightPositions']['square'])
    time.sleep(2)
    print('\n')

    print("Testing piecesPositions['knightPositions']['rc'] from previous input. Expect to receive 23")   
    print(piecesPositions['knightPositions']['rc'])
    time.sleep(2)
    print('\n')

    print("Testing str(piecesPositions['knightPositions']['rc'])[0] from previous input. Expect to receive r1=2 r2=3")  
    r1=str(piecesPositions['knightPositions']['rc'])[0]
    r2=str(piecesPositions['knightPositions']['rc'])[1]
    print('r1='+r1+' r2='+r2)
    time.sleep(2)
    print('\n')

    print("Testing piecesPositions['knightPositions']['alpha'] from previous input. Expect to receive C2")    
    print(piecesPositions['knightPositions']['alpha'])
    time.sleep(2)
    print('\n')

    print("Testing piecesPositions['pawnsPositions']['square'] from previous input. Expect to receive [0,17]")    
    print(piecesPositions['pawnsPositions']['square'])
    time.sleep(2)
    print('\n')

    print("Testing piecesPositions['pawnsPositions']['rc'] from previous input. Expect to receive [11,32]")    
    print(piecesPositions['pawnsPositions']['rc'])
    time.sleep(2)
    print('\n')

    print("Testing piecesPositions['pawnsPositions']['alpha'] from previous input. Expect to receive [A1,B3]")    
    print(piecesPositions['pawnsPositions']['alpha'])
    time.sleep(2)
    print('\n')

    print("Testing piecesPositions['pawnsPositions']['Length'] from previous input. Expect to receive 2")    
    print(piecesPositions['pawnsPositions']['Length'])
    time.sleep(2)
    print('\n')


    print('processing_prompt_data(x)')
    print("Fake user input '10 0 17 20 44 50 50 20 '. Expected to receive {'knightSquare': '10', 'pawnsPositions': ['0 17 20 44 50']}")
    print(processing_prompt_data('10 0 17 20 44 50 50 20 '))
    time.sleep(2)
    print('------------------------------------------------------------')    
    print('\n')


    # More tests

    
    print('\n')
    print("End of tests")
# ------------------------------------------------------------ 

def valid_steps (r1, c1):
    # Pass trough RC-[1-8]x[1-8] coordinates and return an dictionary of knight valid steps
    # A valid knigth step should folow this rule: |r1-r2|+|c1-c2|=3 whith r1!=r2 and c1!=c2. http://www.inf.ufsc.br/grafos/temas/hamiltoniano/cavalo.htm
    validSteps={}
    for r2 in range(1,9):
        for c2 in range (1,9):            
            if (abs(r1-r2)+abs(c1-c2)==3) and r1!=r2 and c1!=c2: # Valid Knight mouvement
                validSteps[str(r2)+str(c2)]=[r2,c2]
    
    return validSteps
# ------------------------------------------------------------

def processing_prompt_data(x):
    
    userInput = x.split(' ')
    piecesPositions={}
    knightPositions={}
    pawnsPositions={}
    coordinatesMap=get_map_coordinates()

    knightPositionSq = userInput.pop(0)
    pawnsPositionSq  = sorted(userInput)

    mapSquareRC = coordinatesMap['mapSquareRC']
    charMap     = coordinatesMap['charMap']

    # knight
    knightPositions['square']=knightPositionSq
    knightPositions['rc']=mapSquareRC[knightPositionSq]
    knightPositions['alpha']=charMap[knightPositionSq]
    

    # pawns    
    pawnsSq={}
    pawnsRC={}
    pawnsAlpha={}
    for pawnsSquares in pawnsPositionSq:
        if pawnsSquares is not '': # Cleaning noises (empty spaces, repeated values). Ex: Enter at least 2 numbers between 0-63>10    20   30   40
            pawnsSq[pawnsSquares]=0 # Not captured - 0. Captured - 1.
            pawnsRC[mapSquareRC[pawnsSquares]]=0 # Mapping RC coordiates
            pawnsAlpha[charMap[pawnsSquares]]=0


    pawnsRC = dict(sorted(pawnsRC.items())) # Sort asc
    pawnsLength = len(pawnsRC)
    
    pawnsPositions['square']=pawnsSq
    pawnsPositions['rc']=pawnsRC
    pawnsPositions['alpha']=pawnsAlpha
    pawnsPositions['Length']=pawnsLength
    


    piecesPositions['knightPositions']=knightPositions
    piecesPositions['pawnsPositions']=pawnsPositions
    
    return piecesPositions
# ------------------------------------------------------------

def get_map_coordinates():
    # 1 - Initialization: Map square number (0-63) to RC[1-8] (Row-Col) coordinates. {'0': 11, '1': 12, ... and apha coodinate [A-H]
    coordinatesMap={}  
    mapSquareRC={}
    charMap={}
    characters = string.ascii_uppercase
    sq=0
    for mapR in range(1,9):
        for mapC in range (1,9):
            rc=int(str(mapR)+str(mapC))
            mapSquareRC[str(sq)]=rc
            charMap[str(sq)]=str(characters[mapC-1])+str(mapR)
            sq += 1  
    coordinatesMap['mapSquareRC'] =   mapSquareRC     
    coordinatesMap['charMap']     =   charMap  
    return   coordinatesMap        
# ------------------------------------------------------------

def get_board():
    board={}
    for r in range(1,9):
        for c in range(1,9):
            rc = str(r)+str(c)
            board[rc]=0
    return board
# ------------------------------------------------------------

def move_knight (r1,c1, pawnsRC):
    board = get_board()
    result={}
    pawnCaptured={}
    trackPath={} 
    # FIRST APPROACH - UNTIL THE BOARD WAS NOT TRAVERSED TROUGH IN ALL CELLS (THE PAWNS ARE IN SOME OF THEM)
    # repeat the loop untill the board is full. Means sum = 64
    # during the track - capture pawns. (Set pawns = 1)
    nb=1
    while sum(board.values()) < len(board):        
        # Knight current position
        r1c1 = str(r1)+str(c1)
        # set the current RC = 1 (token), in order to avoid retrun in this cell
        board[r1c1]=1
        trackPath[nb]=r1c1

        # retrieve all possible cells for the next step
        validSteps = valid_steps(int(r1),int(c1))

        # remove those RC already token
        validStepsClean = remove_Token_RC(validSteps,board)
        

        # TODO - Must find a better way to choose the NEXT VALID STEP. WHY? HOW?
        # TODO - Firs approach - Let's make it RANDOM AND until all pawns are captured.

        # Retrieve a random RC       
        # rc=random_RC(validStepsClean) # Remove previus step token. 
        rc=random_RC(validSteps) # Remove RESTRINCTION OF previus step token. 

        # Move knight to the next random cell
        r1=rc[0]
        c1=rc[1]
        r1c1 = str(r1)+str(c1)


        # Capture a pawn, if its is teh case
        # if there is a pawn set pawnCaptured[r1c1]=r1c1  
        pawnCaptured=capture_pawns(r1c1,pawnsRC,pawnCaptured)
        
        if len(pawnsRC)==len(pawnCaptured):
            print ('All captured')
            break
        
        nb +=1        
              
        
    
    result['trackPath']=trackPath
    result['pawnCaptured']=pawnCaptured
    return result
# ------------------------------------------------------------

def random_RC(validSteps):
    iter(validSteps.values())
    keys = list(validSteps)
    random.shuffle(keys)      
    validStepsTemp = {}
    for key in keys: validStepsTemp[key] = validSteps[key]        
    rc = next(iter(validStepsTemp.values()))
    
    return rc
# ------------------------------------------------------------

def remove_Token_RC(validSteps,board):
    d = dict(validSteps)   
    for key in board:
        if board[key]==1:
            d.pop(key, None)
    return d
# ------------------------------------------------------------

def capture_pawns(r1c1,pawnsRC,pawnCaptured):

    for k, v in pawnsRC.items():
        
        if int(r1c1)==int(k):
            # print ('Pawn captured at '+str(r1c1)+'-'+str(k))
            pawnsRC[k]=1
            pawnCaptured[int(r1c1)]=int(r1c1)

    return pawnCaptured