from repository import *

x = input('Choose 2 numbers between 0-63>')

piecesPositions = processing_prompt_data(x)
coordinatesMap = get_map_coordinates()
mapSquareRC = coordinatesMap['mapSquareRC']
charMap = coordinatesMap['charMap']

r1=str(piecesPositions['knightPositions']['rc'])[0] # Get RC knight coordinate r1
c1=str(piecesPositions['knightPositions']['rc'])[1] # Get RC knight coordinate c1


pawnsRC=piecesPositions['pawnsPositions']['rc']
pawnsLength=piecesPositions['pawnsPositions']['Length']

# print the trackPath
result = move_knight(int(r1),int(c1),pawnsRC)
trackPath=result['trackPath']
pawnCaptured=result['pawnCaptured']

for k, v in trackPath.items():    
    sq=str({v:k for k, v in mapSquareRC.items()}[int(v)])
    print(str(sq)+'-'+str(charMap[sq])) 
print ('End of Tour')